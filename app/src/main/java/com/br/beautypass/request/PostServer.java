package com.br.beautypass.request;

import android.os.StrictMode;

import com.br.beautypass.manager.PropertiesController;
import com.br.beautypass.util.RequestUtils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class PostServer {

    private String URL = PropertiesController.getPropertie("url_server");
    protected HashMap<String, String> HEADERS;

    protected JSONObject sendPost(String json, String URI) {

        JSONObject jsonToReturn = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            InputStream inputStream = null;
            String contentType = null;
            HttpURLConnection request = (HttpURLConnection) new URL(URL+URI).openConnection();

            try {

                request.setDoOutput(true);
                request.setDoInput(true);

                request.setRequestProperty("Content-Type", "application/json");
                request.setRequestMethod("POST");
                request.connect();

                try (OutputStream outputStream = request.getOutputStream()) {
                    outputStream.write(json.getBytes("UTF-8"));
                }

                // Caso você queira usar o código HTTP para fazer alguma coisa, descomente esta linha.
                inputStream = request.getInputStream();
                contentType = request.getHeaderField("Content-Type");

                if (inputStream != null) {

                    String charSet = RequestUtils.getCharSet(contentType);
                    //Obtem o JSON
                    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream, charSet))) {
                        JSONParser parser = new JSONParser();
                        Object obj = parser.parse(fileReader);
                        jsonToReturn = (JSONObject) obj;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } finally {
                request.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return jsonToReturn;
    }

    private String readResponse(HttpURLConnection request){
        ByteArrayOutputStream os = null;
        try (InputStream is = request.getInputStream()) {
            os = new ByteArrayOutputStream();
            int b;
            while ((b = is.read()) != -1) {
                os.write(b);
            }
        }catch (Exception e){

        }
        return new String(os.toByteArray());
    }

}
