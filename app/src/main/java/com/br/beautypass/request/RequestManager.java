package com.br.beautypass.request;

import com.br.beautypass.manager.UserManager;
import com.google.android.gms.maps.model.LatLng;

import org.json.simple.JSONObject;

public class RequestManager extends RequestServer{

    public JSONObject getStores(){
        LatLng userLocale = UserManager.getInstance().getLatLngUser();
        super.HEADERS.put("coordenada-x", Double.toString(userLocale.latitude));
        super.HEADERS.put("coordenada-y", Double.toString(userLocale.longitude));

        return super.getJSONByURL("/stores");
    }

    public JSONObject getStores(String seachText){
        LatLng userLocale = UserManager.getInstance().getLatLngUser();
        super.HEADERS.put("coordenada-x", Double.toString(userLocale.latitude));
        super.HEADERS.put("coordenada-y", Double.toString(userLocale.longitude));
        super.HEADERS.put("search", seachText);

        return super.getJSONByURL("/stores");
    }

    public JSONObject getServicesFromStore(String storeId){
        super.HEADERS.put("idStore", storeId);
        return super.getJSONByURL("/serviceResource");
    }

    public JSONObject getCalendarService(String serviceId){
        super.HEADERS.put("serviceId", serviceId);
        return super.getJSONByURL("/serviceCalendar");
    }

    public JSONObject getUser(){
        super.HEADERS.put("userLocation", UserManager.getInstance().getXY());
        return super.getJSONByURL("/user");
    }

    public JSONObject getCalendarUser(){
        super.HEADERS.put("userId", Integer.toString(UserManager.getInstance().getIdUser()));
        return super.getJSONByURL("/userCalendar");
    }

}
