package com.br.beautypass.request;

import android.os.StrictMode;

import com.br.beautypass.manager.PropertiesController;
import com.br.beautypass.util.RequestUtils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Set;

public class RequestServer {

    private String URL = PropertiesController.getPropertie("url_server");
    protected HashMap<String, String> HEADERS;

    public RequestServer(){
        HEADERS = new HashMap<>();
        HEADERS.put("Accept", "application/json");
        HEADERS.put("Content-Type", "application/json;charset=utf-8");
    }

    protected JSONObject getJSONByURL(String uri) {

        JSONObject jsonToReturn = null;
        InputStream inputStream = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String contentType = null;
        HttpURLConnection apiInfoConnection;
        try {

            apiInfoConnection = (HttpURLConnection) new URL(URL+uri).openConnection();
            apiInfoConnection.setRequestMethod("GET");

            Set<String> headersKey = HEADERS.keySet();

            for (String key : headersKey){
                apiInfoConnection.setRequestProperty(key, HEADERS.get(key));
            }

            if (apiInfoConnection != null && apiInfoConnection.getResponseCode() != 200) {
                return null;
            }
            inputStream = apiInfoConnection.getInputStream();
            contentType = apiInfoConnection.getHeaderField("Content-Type");
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }

        if (inputStream != null) {

            String charSet = RequestUtils.getCharSet(contentType);
            //Obtem o JSON
            try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream, charSet))) {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(fileReader);
                jsonToReturn = (JSONObject) obj;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonToReturn;
    }

}
