package com.br.beautypass.manager;

import android.content.Context;

import com.br.beautypass.dao.UserDAO;
import com.br.beautypass.model.User;
import com.br.beautypass.request.RequestManager;
import com.br.beautypass.request.RequestServer;
import com.google.android.gms.maps.model.LatLng;

import org.json.simple.JSONObject;

public class UserManager extends Manager{

    private User user = new User(-1, "guest");;
    private UserDAO userDAO;


    //Valores padrões de localização (Vila Olimpia)
    private double positionX = -23.600426,
                    positionY = -46.677061;

    private static UserManager userManager;

    public void loadUser(Context context){
        userDAO = new UserDAO(context);
        User user = userDAO.getUser();

        if (user == null) {
            new Thread() {
                @Override
                public void run() {
                    User user = requestNewUser();
                    if (user != null) {
                        userDAO.insertUser(user);
                        setUser(user);
                    }
                }
            }.start();
        }else{
            setUser(user);
        }
    }

    public static UserManager getInstance(){
        if (userManager == null){
            userManager = new UserManager();
        }
        return userManager;
    }

    public LatLng getLatLngUser(){
        return new LatLng(positionX, positionY);
    }

    public void setLocaleUser(double x, double y){
        positionX = x;
        positionY = y;
    }

    public String getXY(){
        return positionX+", "+positionY;
    }

    public String getName(){
        return user.getName();
    }

    public void setUser(User user){
        this.user = user;
    }

    public int getIdUser(){ return user.getId();}

    public User requestNewUser(){

        RequestManager requestManager = new RequestManager();
        JSONObject userJSON = requestManager.getUser();
        User user = null;

        if (userJSON != null && userJSON.get("sucess") != "true"){

            JSONObject userDate = (JSONObject) userJSON.get("newUser");

            int idUser = Integer.parseInt(userDate.get("id").toString());
            String name = userDate.get("name").toString();
            user = new User(idUser, name);

        }

        return user;
    }

}
