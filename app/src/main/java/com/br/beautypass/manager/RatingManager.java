package com.br.beautypass.manager;

import android.os.Looper;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.dialog.DialogRaitingService;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.request.PostManager;

import org.json.simple.JSONObject;

public class RatingManager  extends Manager {

    private static RatingManager instance;
    private ScheduledService service;
    private int value;
    private DialogRaitingService dialog;

    public static RatingManager getInstance(){
        if (instance == null){
            instance = new RatingManager();
        }
        return instance;
    }

    public void postRaiting(ScheduledService service, int value, DialogRaitingService dialogRaitingService) {
        this.service = service;
        this.value = value;
        this.dialog = dialogRaitingService;

        Home.getHome().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                process();
            }
        });

    }

    private void callBackError(String errorMessage){
        dialog.close();
        DialogRaitingService dialogRaitingError = new DialogRaitingService(errorMessage);
        dialogRaitingError.show();
    }

    private void callBackSucess(){
        dialog.close();
        DialogRaitingService dialogRaitingSucess = new DialogRaitingService(service);
        dialogRaitingSucess.show();
    }

    @Override
    public void process(){

        JSONObject jsonToPost = new JSONObject();
        jsonToPost.put("id", service.getId());
        jsonToPost.put("value", value);

        PostManager postManager = new PostManager();
        JSONObject jsonResponse = postManager.postRating(jsonToPost);

        if (jsonResponse != null){

            Object sucessObj = jsonResponse.get("sucess");
            Object text = jsonResponse.get("responseTxt");
            if (sucessObj != null){

                if (sucessObj.toString().equals("true")){
                    //Atualiza a lista de lojas
                    StoreManager.getInstance().process();
                    callBackSucess();
                }else{
                    if (text != null) {
                        callBackError(text.toString());
                    }else{
                        callBackError("Erro no servidor");
                    }
                }

            }else{
                callBackError("Erro ao formatar os dados no servidor");
            }

        }else{
            callBackError("Erro ao obter a resposta do servidor");
        }

    }

}
