package com.br.beautypass.manager;

import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.DateService;
import com.br.beautypass.model.TimeService;
import com.br.beautypass.request.RequestManager;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class DateStoreManager extends Manager{

    private static DateStoreManager dateManager;
    private int serviceId;
    private Details_service detailsServiceToCallBack;

    public static DateStoreManager getInstance(){
        if (dateManager == null){
            dateManager = new DateStoreManager();
        }
        return dateManager;
    }

    public void processServiceDate(int serviceId, Details_service detailsServiceToCallBack){
        this.serviceId = serviceId;
        this.detailsServiceToCallBack = detailsServiceToCallBack;

        new Thread() {
            @Override
            public void run() {
                getServicesDate();
            }
        }.start();

    }

    public void callBackDetailsService(ArrayList<DateService> listOfDate){
        if (detailsServiceToCallBack != null)
            detailsServiceToCallBack.callBackDateSelect(listOfDate);
    }
    public void callBackErrorDetailsService(String message){
        if (detailsServiceToCallBack != null)
            detailsServiceToCallBack.callBackErrorDateSelect(message);
    }

    public void getServicesDate(){

        RequestManager requestManager = new RequestManager();
        JSONObject jsonResponse = requestManager.getCalendarService(Integer.toString(serviceId));

        ArrayList<DateService> listOfServices = null;
        if (jsonResponse != null) {
            Object sucessObj = jsonResponse.get("sucess");
            if (sucessObj != null && sucessObj.toString().equals("true")) {

                listOfServices = new ArrayList<>();
                JSONArray arrTime = (JSONArray) jsonResponse.get("calendar");
                for (int index = 0; index < arrTime.size(); index++) {

                    JSONObject jsonDate = (JSONObject) arrTime.get(index);
                    DateService dateService = new DateService();
                    dateService.setDate(jsonDate.get("date").toString());
                    dateService.setDayOfWeek(jsonDate.get("dayOfWeek").toString());

                    ArrayList<TimeService> listOfTimes = new ArrayList<>();
                    JSONArray timesArr = (JSONArray) jsonDate.get("vagancies");

                    for (int timesIndex = 0; timesIndex < timesArr.size(); timesIndex++) {
                        JSONObject jsonObjTime = (JSONObject) timesArr.get(timesIndex);
                        TimeService timeService = new TimeService();
                        Long timeDate = Long.parseLong(jsonObjTime.get("timeDate").toString());
                        timeService.setTimeDate(timeDate);
                        timeService.setTime(jsonObjTime.get("time").toString());
                        listOfTimes.add(timeService);
                    }

                    dateService.setListOfTime(listOfTimes);
                    listOfServices.add(dateService);
                }
                if (!listOfServices.isEmpty()) {
                    callBackDetailsService(listOfServices);
                } else {
                    callBackErrorDetailsService("Não há horarios disponiveis");
                }
            } else if (sucessObj != null) {
                callBackErrorDetailsService("Erro ao processar os dados no servidor");
            } else {
                callBackErrorDetailsService("Erro ao obter os dados");
            }
        }else{
            callBackErrorDetailsService("Erro ao obter os dados");
        }

    }

}
