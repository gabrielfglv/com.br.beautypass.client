package com.br.beautypass.manager;

import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.store.Detail_store_fragment;
import com.br.beautypass.model.Service;
import com.br.beautypass.request.RequestManager;
import com.br.beautypass.util.Logger;
import com.br.beautypass.util.StoreUtils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class ServiceManager extends Manager{

    private static ServiceManager serviceManager;
    private static String idToSearch;
    private ArrayList<Service> listOfServices;

    public static ServiceManager getInstance(String id){
        idToSearch = id;
        return getInstance();
    }

    public static ServiceManager getInstance(){
        if (serviceManager == null){
            serviceManager = new ServiceManager();
        }
        return serviceManager;
    }

    public void callFragmentSucess(ArrayList<Service> listOfServices){
        isReady = true;
        loading = false;
        for (Fragment_menu fragment_menu : fragments_to_call_back) {
            if (fragment_menu instanceof Detail_store_fragment) {
                ((Detail_store_fragment) fragment_menu).callBackReady(listOfServices);
            }else{
                fragment_menu.callBackReady();
            }
        }
    }

    @Override
    protected void process(){
        new Thread() {
            @Override
            public void run() {

                RequestManager requestServer = new RequestManager();
                JSONObject listOfServices = requestServer.getServicesFromStore(idToSearch);
                if (listOfServices != null) {
                    ServiceManager.getInstance().setServices(listOfServices);
                } else {
                    ServiceManager.getInstance().callFragmentError("Erro ao obter os serviços da loja, contate o adminsitrador.");
                }

            }
        }.start();
    }

    public void setServices(JSONObject jsonService){

        try {
            boolean sucess = (Boolean)jsonService.get("sucess");

            //Valida se o servidor retornou corretamente
            if (sucess || true){

                Long lengthStores = (Long)jsonService.get("sizeServices");
                if (lengthStores > 0) {
                    buildServices(jsonService);

                    //Valida se foi possível inserir as lojas
                    if (listOfServices.isEmpty()){
                        callFragmentError("Erro ao processar os dados dos serviços");
                        Logger.erroMessage("Erro ao processar os dados dos serviços");
                    }else {
                        ServiceManager.getInstance().callFragmentSucess(listOfServices);
                        isReady = true;
                    }
                }else{
                    callFragmentError("Não há serviços cadastrados na loja");
                    Logger.erroMessage("Servidor não retornou serviços");
                }

            }else{
                Logger.erroMessage("Erro ao obter os serviços no servidor");
            }

        }catch (Exception e){
            callFragmentError("Erro ao processar os dados dos serviços");
            Logger.erroMessage(e, "Erro ao processar os dados dos serviços");
            e.printStackTrace();
        }

    }

    private void buildServices(JSONObject jsonServices){

        listOfServices = new ArrayList<>();
        JSONArray listOfJsonServices = (JSONArray) jsonServices.get("services");

        for (int index = 0; index < listOfJsonServices.size(); index++){
            JSONObject jsonService = (JSONObject) listOfJsonServices.get(index);
            Service service = StoreUtils.getServiceByJSON(jsonService);
            if (service.isValid())
                listOfServices.add(service);
        }

    }


}
