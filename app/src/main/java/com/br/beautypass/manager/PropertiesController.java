package com.br.beautypass.manager;

import java.util.HashMap;

public class PropertiesController {

    private static HashMap<String, String> listOfProperties = new HashMap<String, String>();

    public static boolean setPropertie(String key, String value){
        if (!listOfProperties.containsKey(key)) {
            listOfProperties.put(key, value);
            return true;
        }
        return false;
    }

    public static boolean updatePropertie(String key, String value){
        if (listOfProperties.containsKey(key)) {
            listOfProperties.put(key, value);
            return true;
        }
        return false;
    }

    public static String getPropertie(String key){
        String response = null;
        if (key != null && listOfProperties.containsKey(key)){
            response = listOfProperties.get(key);
        }
        return response;
    }

}
