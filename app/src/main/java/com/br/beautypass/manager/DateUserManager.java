package com.br.beautypass.manager;

import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.menu.Calendar_fragment;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.model.Store;
import com.br.beautypass.request.RequestManager;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class DateUserManager  extends Manager{

    private static DateUserManager dateManager;
    private boolean offline = false;

    public static DateUserManager getInstance(){
        if (dateManager == null){
            dateManager = new DateUserManager();
        }
        return dateManager;
    }

    @Override
    public void process(){
        new Thread() {
            @Override
            public void run() {
                processDates();
            }
        }.start();
    }

    @Override
    public void callFragmentSucess(){
        for (Fragment_menu fragment_menu : fragments_to_call_back)
            fragment_menu.callBackReady();
        fragments_to_call_back.clear();
    }

    @Override
    public void callFragmentError(String message){

    }

    public void callBackDetailsService(ArrayList<ScheduledService> listOfDate){
        for(Fragment_menu fragment_menu : fragments_to_call_back){
            if (fragment_menu instanceof Calendar_fragment){
                ((Calendar_fragment)fragment_menu).callBackReady(listOfDate, offline);
            }
        }
        fragments_to_call_back.clear();
    }

    public void processDates(){

        RequestManager requestManager = new RequestManager();
        JSONObject jsonResponse = requestManager.getCalendarUser();

        String strResponse = formatResponseManager(jsonResponse);
        if (strResponse == null){
            CachedJSONController.saveOrUpdateJSON("userCalendar", jsonResponse);
        }else{
            offline = true;
            JSONObject jsonBkp = CachedJSONController.getJSONObjectById("userCalendar");

            if (jsonBkp == null
                    || formatResponseManager(jsonBkp) != null){
                callFragmentError(strResponse);
            }

        }

    }

    private String formatResponseManager(JSONObject jsonResponse){
        ArrayList<ScheduledService> listOfServices = null;

        if (jsonResponse != null){

            listOfServices = new ArrayList<>();
            Object sucessObj = jsonResponse.get("sucess");

            if (sucessObj != null && sucessObj.toString().equals("true")) {

                JSONArray arrServices = (JSONArray) jsonResponse.get("services");

                if (arrServices != null) {
                    listOfServices = getUserDatesByJSON(arrServices);
                    callBackDetailsService(listOfServices);
                } else {
                    return "Erro no retorno de dados do servidor";
                }
            }else{
               return "Erro no retorno de dados do servidor";
            }

        }else{
            return "Erro ao obter os dados do servidor";
        }
        isReady = false;
        loading = false;
        return null;
    }

    public void initProcess(Fragment_menu fragment){
        fragments_to_call_back.add(fragment);
        process();
    }

    public ArrayList<ScheduledService> getUserDatesByJSON(JSONArray arrServices){

        ArrayList<ScheduledService> listOfServices = new ArrayList<>();

        for (int index = 0; index < arrServices.size(); index++) {

            JSONObject jsonDate = (JSONObject) arrServices.get(index);
            ScheduledService newService = new ScheduledService();

            newService.setId(Integer.parseInt(jsonDate.get("id").toString()));

            JSONObject serviceObj = (JSONObject) jsonDate.get("service");
            newService.setNameService(serviceObj.get("name").toString());
            newService.setIdService(Integer.parseInt(serviceObj.get("id").toString()));

            newService.setValue(Double.parseDouble(jsonDate.get("value").toString()));
            newService.setConfirmed(Integer.parseInt(jsonDate.get("confirmed").toString()));
            newService.setScore(Integer.parseInt(jsonDate.get("score").toString()));
            newService.setExecuted(jsonDate.get("executed").toString().equals("true"));
            newService.setStatusStr(jsonDate.get("statusStr").toString());

            JSONObject jsonStore = (JSONObject) jsonDate.get("store");
            Store store = new Store();
            store.setId(Integer.parseInt(jsonStore.get("id").toString()));
            store.setTitle(jsonStore.get("title").toString());
            store.setAdress(jsonStore.get("adress").toString());
            store.setCep(jsonStore.get("cep").toString());
            store.setTelefone(jsonStore.get("tel").toString());
            store.setEmail(jsonStore.get("email").toString());
            store.setIdImage(Integer.parseInt(jsonStore.get("imageId").toString()));

            newService.setStore(store);

            JSONArray options = (JSONArray) jsonDate.get("options");
            ArrayList<ServiceOption> listOfOptions = new ArrayList<>();
            for (int indexOp = 0; indexOp < options.size(); indexOp++) {
                JSONObject option = (JSONObject) options.get(indexOp);
                ServiceOption servOp = new ServiceOption();
                servOp.setId(Integer.parseInt(option.get("id").toString()));
                servOp.setTitle(option.get("title").toString());
                servOp.setValor(Double.parseDouble(option.get("value").toString()));
                listOfOptions.add(servOp);
            }
            newService.setListOfOptions(listOfOptions);

            JSONObject time = (JSONObject) jsonDate.get("date");
            newService.setDateString(time.get("simplificateDate").toString());

            listOfServices.add(newService);
        }

        return listOfServices;
    }

}

