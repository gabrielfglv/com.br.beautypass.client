package com.br.beautypass.manager;

import com.br.beautypass.client.Contact;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.menu.Map_fragment;
import com.br.beautypass.request.GetMapsServer;
import com.br.beautypass.request.RequestManager;
import com.google.android.gms.maps.model.LatLng;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MapManager extends Manager {

    private static MapManager instance;
    private String searchAdress;

    public static MapManager getInstance(){
        if (instance == null){
            instance = new MapManager();
        }
        return instance;
    }

    public LatLng process(final String searchText, Fragment_menu fragmentToCallBack){

        if (!loading) {
            loading = true;
            fragments_to_call_back.add(fragmentToCallBack);
            this.searchAdress = searchText;

            GetMapsServer getMapsServer = new GetMapsServer();

            JSONObject jsonResponse = getMapsServer.getJSONByURL(searchAdress);

            if (jsonResponse != null){

                JSONArray arrResult = (JSONArray) jsonResponse.get("results");

                if (arrResult != null){

                    if (!arrResult.isEmpty()){

                        JSONObject locale = (JSONObject) arrResult.get(0);
                        JSONObject geometry = (JSONObject) locale.get("geometry");
                        JSONObject coords = (JSONObject) geometry.get("location");

                        float coordX = -1;
                        float coordY = -1;
                        try {
                            coordX = Float.parseFloat(coords.get("lat").toString());
                            coordY = Float.parseFloat(coords.get("lng").toString());

                        }catch (Exception e){
                            return null;
                        }

                        if (coordX != -1 && coordY != -1) {
                            loading = false;
                            return new LatLng(coordX, coordY);
                        }

                    }
                }
            }
        }
        loading = false;
        return null;
    }


}
