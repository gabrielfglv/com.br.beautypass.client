package com.br.beautypass.manager;


import com.br.beautypass.client.Home;
import com.br.beautypass.dao.ServicesDAO;

import org.json.simple.JSONObject;


public class CachedJSONController {

    private static ServicesDAO servicesDAO;

    public static boolean saveOrUpdateJSON(String key, JSONObject json){

        servicesDAO = new ServicesDAO(Home.getHome().getBaseContext());


        JSONObject jsonReturn = servicesDAO.getJSONCached(key);

        boolean sucess = false;
        if (jsonReturn == null){
            sucess = servicesDAO.insertJSON(key, json);
        }else{
            sucess = servicesDAO.updateJSON(key, json);
        }

        return sucess;
    }

    public static JSONObject getJSONObjectById(String id){
        servicesDAO = new ServicesDAO(Home.getHome().getBaseContext());
        return servicesDAO.getJSONCached(id);
    }

}
