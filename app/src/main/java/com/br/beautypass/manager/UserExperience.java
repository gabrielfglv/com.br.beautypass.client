package com.br.beautypass.manager;

import androidx.annotation.NonNull;

import com.br.beautypass.client.Home;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.InterfaceUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class UserExperience {

    private static ArrayList<Service> listOfServicesUser = new ArrayList<>();
    private static int limit = 3;

    public static void enterService(Service service, Store store){
        service.setStore(store);
        if(listOfServicesUser.size() < limit && !hasService(service)){
            listOfServicesUser.add(service);
        }
    }

    public static boolean finalizeService(Service service){
        for (int index = 0; index < listOfServicesUser.size(); index++){
            if (listOfServicesUser.get(index).getId() == service.getId()){
                listOfServicesUser.remove(index);
                return true;
            }
        }
        return false;
    }

    private static boolean hasService(Service service){
        for (int index = 0; index < listOfServicesUser.size(); index++){
            if (listOfServicesUser.get(index).getId() == service.getId()){
                return true;
            }
        }
        return false;
    }

    public static ArrayList<Service> getListOfServicesUser(){
        return listOfServicesUser;
    }

    public static void enterStore(Store store){
/*
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> storeMap = new HashMap<>();
        storeMap.put("id", store.getId());
        storeMap.put("name", store.getTitle());

        db.collection("users")
                .add(storeMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        InterfaceUtils.showMensage("Sucesso no fireStore", Home.getHome());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        InterfaceUtils.showMensage("Erro no fireStore", Home.getHome());
                    }
                });/*/

    }

}
