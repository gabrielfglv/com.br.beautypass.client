package com.br.beautypass.manager;

import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.request.PostManager;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class MarkServiceManager extends Manager{

    private static MarkServiceManager instance;
    private Details_service detailsServiceToCallBack;

    //Atributos do serviço à ser marcado
    private int serviceId;
    private Long dateTime;
    private double value;
    private ArrayList<ServiceOption> serviceOptions;

    public static MarkServiceManager getInstance(){
        if (instance == null){
            instance = new MarkServiceManager();
        }
        return instance;
    }

    public void sendServiceMarker(int serviceId, Long dateTime, double value,
                                  ArrayList<ServiceOption> serviceOptions, Details_service detailsServiceToCallBack){
        this.serviceId = serviceId;
        this.dateTime = dateTime;
        this.value = value;
        this.serviceOptions = serviceOptions;
        this.detailsServiceToCallBack = detailsServiceToCallBack;

        if (dateTime != null && serviceOptions != null) {
            new Thread() {
                @Override
                public void run() {
                    processData();
                }
            }.start();
        }

    }

    private void processData(){

        int idUser = UserManager.getInstance().getIdUser();

        if (idUser != -1) {

            JSONObject jsonService = new JSONObject();
            jsonService.put("serviceId", this.serviceId);
            jsonService.put("dateTime", this.dateTime);
            jsonService.put("value", this.value);
            jsonService.put("idUser", idUser);

            JSONArray optionsSelected = new JSONArray();
            for(ServiceOption serviceOption : serviceOptions){
                optionsSelected.put(serviceOption.toJSONObject());
            }

            jsonService.put("options", optionsSelected);

            PostManager postManager = new PostManager();
            JSONObject response = postManager.postServiceTime(jsonService);


            if (response != null && !response.isEmpty()){

                Object sucessObj = response.get("sucess");
                if (sucessObj != null) {

                    boolean sucess = sucessObj.toString().equals("true");
                    String txtInfo = response.get("responseTxt").toString();

                    if(sucess){
                        callBackSucess();
                    }else{
                        callFragmentError("Houve um erro no servidor: "+txtInfo);
                    }

                }else{
                    callFragmentError("Houve um erro ao processar os dados no servidor");
                }

            }else{
                callBackFail("Erro ao obter as informações do servidor");
            }

        }else{
            callBackFail("Usuario não está logado");
        }

    }

    private void callBackSucess(){
        this.detailsServiceToCallBack.callBackFinalizeSucessService();
    }

    private void callBackFail(String message){
        this.detailsServiceToCallBack.callBackFinalizeErrorService(message);
    }

}
