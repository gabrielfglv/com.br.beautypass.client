package com.br.beautypass.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;

public class CalcUtils {

    public static double roundNumber(double val, int qtCasas){
        BigDecimal bd = new BigDecimal(val).setScale(qtCasas, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

    public static String getFormatedValueMoney(Double valor){
        String value = "R$ " + Double.toString(valor);
        value = value.replace('.',',');

        String []afterPoint = value.split(",");
        if (afterPoint.length > 1 && afterPoint[1].length() == 1){
            value = value.concat("0");
        }else{
            value = value.concat(",00");
        }

        return value;
    }

    public static String completeNumber(int number) {
        if (number < 10) {
            return "0"+number;
        }
        return Integer.toString(number);
    }

    public static Time getTime(String timeStr){
        String timeSplited[] = timeStr.split(":");
        int hour = Integer.parseInt(timeSplited[0]);
        int minutes = Integer.parseInt(timeSplited[1]);
        return new Time(hour, minutes, 0);
    }
}
