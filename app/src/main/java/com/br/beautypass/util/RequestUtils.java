package com.br.beautypass.util;

public class RequestUtils {


    public static String getCharSet(String contentType){
        String charSet = null;
        if (contentType != null && contentType.contains("charset")){
            String parameters[] = contentType.split(";");
            for (String parameter : parameters){
                int indexCharset = parameter.indexOf("charset=");
                if (indexCharset > -1){
                    charSet = parameter.substring(indexCharset+8);
                    break;
                }
            }
        }
        return charSet;
    }

}
