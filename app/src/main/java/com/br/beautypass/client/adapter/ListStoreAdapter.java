package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;

import com.br.beautypass.client.R;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.MVPUtils;

import java.util.List;
import java.util.Random;


public class ListStoreAdapter extends BaseAdapter {

    private List<Store> listOfStores;
    private final Activity act;

    public ListStoreAdapter(List<Store> stores, Activity act) {
        this.listOfStores = stores;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listOfStores.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfStores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_stores, parent, false);
        Store store = listOfStores.get(position);

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.title);
        TextView description = (TextView) view.findViewById(R.id.description);
        ImageView imagem = (ImageView) view.findViewById(R.id.image);

        //populando as Views
        nome.setText(store.getTitle());
        description.setText(store.getCategoryDescription());

        imagem.setImageResource(MVPUtils.getImageStoreById(store.getIdImage()));

        return view;
    }

}
