package com.br.beautypass.client.fragments.details.services;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListServiceOptionAdapter;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.service.Details_service;

public class List_options_services  extends Details_Element {

    private ListServiceOptionAdapter listAdapter;
    private Details_service detailsPage;

    public List_options_services(Details_service detailsPage) {
        this.detailsPage = detailsPage;
    }

    public ListServiceOptionAdapter getListAdapter() {
        return listAdapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_option_service_fragment, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        final ListView listViewOptions = (ListView) view.findViewById(R.id.listServiceOptions);

        //Altera a altura da lista
        ViewGroup.LayoutParams params = listViewOptions.getLayoutParams();
        params.height = 180 * this.detailsPage.getService().getServiceOption().size();
        listViewOptions.setLayoutParams(params);
        listViewOptions.setScrollContainer(false);

        listAdapter = new ListServiceOptionAdapter(detailsPage);
        listViewOptions.setAdapter(listAdapter);
    }
}