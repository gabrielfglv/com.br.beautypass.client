package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.CalcUtils;

import java.util.ArrayList;

public class ListServiceOptionAdapter extends BaseAdapter {

    private ArrayList<ServiceOption> listOfServiceOption;
    private final Details_service detailsPage;

    public ListServiceOptionAdapter(Details_service detailsPage) {
        this.listOfServiceOption = detailsPage.getService().getServiceOption();
        this.detailsPage = detailsPage;
    }

    @Override
    public int getCount() {
        return listOfServiceOption.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfServiceOption.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public boolean hasSomeoneMarked(){
        boolean marked = false;
        for (ServiceOption serviceOption : listOfServiceOption){
            if (serviceOption.isChecked()){
                marked = true;
                break;
            }
        }
        return marked;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view = detailsPage.getLayoutInflater().inflate(R.layout.activity_item_list_option_service, parent, false);

        final ServiceOption serviceOption = listOfServiceOption.get(position);

        RadioButton radioButton = (RadioButton) view.findViewById(R.id.radioButton);
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);

        TextView valueText = (TextView) view.findViewById(R.id.value);
        valueText.setText(CalcUtils.getFormatedValueMoney(serviceOption.getValor()));
        valueText.setTag(serviceOption);

        CompoundButton option;
        if (detailsPage.getService().isMultiOption()){
            option = checkBox;
            radioButton.setVisibility(View.GONE);
            checkBox.setText(serviceOption.getTitle());
            checkBox.setTag(serviceOption);
            checkBox.setChecked(serviceOption.isChecked());
        }else{
            option = radioButton;
            checkBox.setVisibility(View.GONE);
            radioButton.setText(serviceOption.getTitle());
            radioButton.setTag(serviceOption);
            radioButton.setChecked(serviceOption.isChecked());
        }

        //Clique na option
        option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detailsPage.getService().isMultiOption() && view instanceof CheckBox) {
                    CheckBox check = (CheckBox) view;
                    markSelected((ServiceOption) check.getTag(), check.isChecked());
                }else if (view instanceof RadioButton){
                    RadioButton check = (RadioButton) view;
                    markSelected((ServiceOption) check.getTag(), check.isChecked());
                }
                refreshList();
            }
        });

        refreshList();

        return view;
    }

    public ServiceOption markSelected(ServiceOption option, boolean isChecked){
        ServiceOption serviceToReturn = null;
        if (option != null) {
            // Procura a opção e a marca como selecionado
            for (ServiceOption serviceOption : listOfServiceOption) {
                if (serviceOption.getId() == option.getId()) {
                    serviceOption.setChecked(isChecked);
                    serviceToReturn = serviceOption;
                    if (detailsPage.getService().isMultiOption()){
                        break;
                    }
                }else if (!detailsPage.getService().isMultiOption()){
                    serviceOption.setChecked(false);
                }
            }
        }
        return serviceToReturn;
    }

    public void refreshList(){
        this.notifyDataSetChanged();
        detailsPage.updateFinalValue();
    }

}