package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.options.Option_fragment;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.Store;

import java.util.List;

public class ListSegmentAdapter extends BaseAdapter {

    private Store store;
    private List<Category> listOfCategorys;
    private final Activity act;

    public ListSegmentAdapter(List<Category> listOfCategorys, Store store, Activity act) {
        this.listOfCategorys = listOfCategorys;
        this.act = act;
        this.store = store;
    }

    @Override
    public int getCount() {
        return listOfCategorys.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfCategorys.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_segments, parent, false);
        Category segment = listOfCategorys.get(position);

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.segmentName);
        //populando as Views
        nome.setText(segment.getName());

        return view;
    }

}