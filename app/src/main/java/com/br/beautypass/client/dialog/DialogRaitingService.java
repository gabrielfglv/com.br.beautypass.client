package com.br.beautypass.client.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.manager.RatingManager;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.util.MVPUtils;

public class DialogRaitingService extends Dialog {

    private int value;
    private ScheduledService service;
    private Activity activity;
    private boolean loadingStatus = false,
                sucess = false;

    private LinearLayoutCompat linearLayoutCompat;
    private RelativeLayout loading;
    private Button submit;
    private TableLayout tableLayoutSucess;
    private TextView close,
            title,
            errorText;
    private ImageView scissor1,
            scissor2,
            scissor3,
            scissor4,
            scissor5,
            attencionImage;

    //Sucess constructor
    public DialogRaitingService(ScheduledService service) {
        super(Home.getHome());
        this.service = service;
        this.activity = Home.getHome();
        this.setContentView(R.layout.dialog_ratiting_service);
        sucess = true;

        buildObjects();
        title.setText(activity.getString(R.string.title_raiting));
        linearLayoutCompat.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);

        ImageView storeImage = (ImageView) this.findViewById(R.id.imageStore);
        storeImage.setImageResource(MVPUtils.getImageStoreById(service.getStore().getIdImage()));

        tableLayoutSucess.setVisibility(View.VISIBLE);

    }

    //Error constructor
    public DialogRaitingService(String response) {
        super(Home.getHome());
        this.activity = Home.getHome();
        this.setContentView(R.layout.dialog_ratiting_service);

        buildObjects();
        title.setText(activity.getString(R.string.title_raiting));
        linearLayoutCompat.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);

        attencionImage.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.VISIBLE);
        errorText.setText(response);

    }

    //Raiting constructor
    public DialogRaitingService(int value, ScheduledService service) {
        super(Home.getHome());
        this.value = value;
        this.service = service;
        this.setContentView(R.layout.dialog_ratiting_service);
        this.activity = Home.getHome();

        buildObjects();
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               sendAvaliation();
            }
        });

        scissor1 = (ImageView) this.findViewById(R.id.scissor1);
        scissor2 = (ImageView) this.findViewById(R.id.scissor2);
        scissor3 = (ImageView) this.findViewById(R.id.scissor3);
        scissor4 = (ImageView) this.findViewById(R.id.scissor4);
        scissor5 = (ImageView) this.findViewById(R.id.scissor5);

        scissor1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeVal(1);
            }
        });

        scissor2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeVal(2);
            }
        });

        scissor3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeVal(3);
            }
        });

        scissor4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeVal(4);
            }
        });

        scissor5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeVal(5);
            }
        });

        changeVal(value);
    }

    private void buildObjects(){

        errorText = (TextView) this.findViewById(R.id.errorMessage);
        attencionImage = (ImageView) this.findViewById(R.id.attencion_image);

        loading = (RelativeLayout) this.findViewById(R.id.loadingPanel);

        linearLayoutCompat = (LinearLayoutCompat) this.findViewById(R.id.scissors);
        tableLayoutSucess = (TableLayout) this.findViewById(R.id.tableSucess);

        close = (TextView) this.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                close();
            }
        });


        Button buttonSucess = (Button) this.findViewById(R.id.exitFinal);
        buttonSucess.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                close();
            }
        });

        title = (TextView) this.findViewById(R.id.title);
        linearLayoutCompat = (LinearLayoutCompat) this.findViewById(R.id.scissors);
        tableLayoutSucess = (TableLayout) this.findViewById(R.id.tableSucess);

        submit = (Button) this.findViewById(R.id.submit);
    }

    public int getServiceId(){
        return service.getId();
    }

    public void close(){
        this.cancel();
    }

    public void changeVal(int value){

        if (loadingStatus){
            return;
        }

        this.value = value;
        @DrawableRes int imageScissorDone = R.drawable.scissors_done;
        @DrawableRes int imageScissor = R.drawable.scissors;

        scissor1.setImageResource(imageScissorDone);

        if (value >= 2) {
            scissor2.setImageResource(imageScissorDone);
        }else{
            scissor2.setImageResource(imageScissor);
        }

        if (value >= 3) {
            scissor3.setImageResource(imageScissorDone);
        }else{
            scissor3.setImageResource(imageScissor);
        }

        if (value >= 4) {
            scissor4.setImageResource(imageScissorDone);
        }else{
            scissor4.setImageResource(imageScissor);
        }

        if (value >= 5) {
            scissor5.setImageResource(imageScissorDone);
        }else{
            scissor5.setImageResource(imageScissor);
        }

        switch (value){
            case 1:
                title.setText(activity.getString(R.string.title_raiting_1));
                break;
            case 2:
                title.setText(activity.getString(R.string.title_raiting_2));
                break;
            case 3:
                title.setText(activity.getString(R.string.title_raiting_3));
                break;
            case 4:
                title.setText(activity.getString(R.string.title_raiting_4));
                break;
            case 5:
                title.setText(activity.getString(R.string.title_raiting_5));
                break;
        }

    }

    public void sendAvaliation(){

        if (!loadingStatus) {

            loadingStatus = true;
            title.setText(activity.getString(R.string.title_raiting));

            linearLayoutCompat.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);

            RatingManager.getInstance().postRaiting(service, value, this);

            /*
            linearLayoutCompat.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);

            //Caso seja nulo, houve sucesso
            if (response == null) {
                sucess = true;
            } else {
                attencionImage.setVisibility(View.VISIBLE);
                errorText.setVisibility(View.VISIBLE);
                errorText.setText(response);
            }
             */

        }
    }

    public boolean isSucess() {
        return sucess;
    }

    public int getFinalValue() {
        return value;
    }
}
