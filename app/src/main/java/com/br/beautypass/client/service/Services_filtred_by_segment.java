package com.br.beautypass.client.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListServicesAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.StoreUtils;

import java.util.List;

public class Services_filtred_by_segment extends AppCompatActivity {

    private List<Service> listOfServicesFiltred;
    private Category segment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_filtred_by_segment);

        TextView title = (TextView) findViewById(R.id.title);
        ListView listViewServices = (ListView) findViewById(R.id.listServices);

        ImageView backButton = (ImageView) findViewById(R.id.back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressBack();
            }
        });

        //Recebe as configurações
        Intent intentRecebida = getIntent();
        if (intentRecebida!=null) {
            Bundle bundle = intentRecebida.getExtras();
            if (bundle != null) {

                final Object storeoBJ = bundle.get("store");
                Object segment = bundle.get("segment");

                if (storeoBJ != null && segment != null){

                    final Store store = (Store)storeoBJ;
                    this.segment = (Category)segment;

                    title.setText(this.segment.getName());

                    listOfServicesFiltred = StoreUtils.getServicesFiltredBySegment(store.getListOfServices(), this.segment);

                    //Monta a lista de serviços
                    if (listOfServicesFiltred != null && !listOfServicesFiltred.isEmpty()) {

                        //Altera a altura da lista
                        ViewGroup.LayoutParams params = listViewServices.getLayoutParams();
                        params.height = 236 * listOfServicesFiltred.size();
                        listViewServices.setLayoutParams(params);
                        listViewServices.setScrollContainer(false);

                        ListServicesAdapter adapter = new ListServicesAdapter(listOfServicesFiltred, this);

                        listViewServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent sendIntent = new Intent(Home.getHome(), Details_service.class);
                            Bundle bundleInf= new Bundle();
                            bundleInf.putSerializable("store", store);
                            bundleInf.putSerializable("service", listOfServicesFiltred.get(position));
                            sendIntent.putExtras(bundleInf);
                            startActivity(sendIntent);
                            }

                        });

                        listViewServices.setAdapter(adapter);
                    }

                }else{
                    title.setText("Erro ao obter os dados");
                }

            }
        }

    }

    public void pressBack(){
        this.onBackPressed();
    }

}
