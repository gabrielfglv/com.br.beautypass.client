package com.br.beautypass.client.fragments.options;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.details.Details_Element;

public class Option_fragment extends Details_Element {

    private @DrawableRes int icon;

    public Option_fragment(String title, @DrawableRes int icon){
        this.icon = icon;
        super.setFragmentName(title);
    }

    public @DrawableRes int getIcon(){
        return icon;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_options_fragment, parent, false);

    }



}
