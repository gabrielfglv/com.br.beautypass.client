package com.br.beautypass.client.fragments.details.store;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.fragments.details.services.List_Services_fragment;
import com.br.beautypass.client.fragments.infoFragment.Error_fragment;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;
import com.br.beautypass.manager.ServiceManager;
import com.br.beautypass.manager.UserExperience;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;

import java.io.Serializable;
import java.util.ArrayList;

public class Detail_store_fragment extends Details_Element {

    private Store thisStore;
    private ArrayList<Service> listOfServices;

    private ImageView backImage;

    private ServiceManager serviceManager;
    private List_Services_fragment listFragment;

    public Detail_store_fragment(Store thisStore, Fragment_menu lastWindow){
        this.thisStore = thisStore;
        super.lastWindow = lastWindow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_detail_store, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Set values for view here

        TextView title = (TextView) view.findViewById(R.id.title_store);
        TextView description = (TextView) view.findViewById(R.id.description_store);
        TextView category = (TextView) view.findViewById(R.id.category) ;
        TextView score = (TextView) view.findViewById(R.id.score) ;

        if (thisStore != null){
            title.setText(thisStore.getTitle());
            description.setText(thisStore.getDescription());
            score.setText(thisStore.getScore()+"("+thisStore.getQtRaitings()+")");
            category.setText(thisStore.getCategoryDescription()+ " • ");
            UserExperience.enterStore(thisStore);
        }

        backImage = (ImageView) view.findViewById(R.id.back);
        backImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backToFatherWindow();
            }
        });

        serviceManager = ServiceManager.getInstance(Integer.toString(thisStore.getId()));
        serviceManager.initProcess(this);

        //Nivel do preco
        TextView priceLvl2 = (TextView) view.findViewById(R.id.price2) ;
        TextView priceLvl3 = (TextView) view.findViewById(R.id.price3) ;
        TextView priceLvl4 = (TextView) view.findViewById(R.id.price4) ;
        TextView priceLvl5 = (TextView) view.findViewById(R.id.price5) ;

        if (thisStore.getPriceLvl() < 2)
            priceLvl2.setTextColor(Color.GRAY);

        if (thisStore.getPriceLvl() < 3)
            priceLvl3.setTextColor(Color.GRAY);

        if (thisStore.getPriceLvl() < 4)
            priceLvl4.setTextColor(Color.GRAY);

        if (thisStore.getPriceLvl() < 5)
            priceLvl5.setTextColor(Color.GRAY);

        this.info_fragment = new Loading_fragment();
        changeFragment(info_fragment);
    }

    private void changeFragment(Fragment_menu toChange){
        this.getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.listCalendar, toChange) // replace flContainer
                .addToBackStack(null)
                .commitAllowingStateLoss();;
    }

    public void callBackReady(ArrayList<Service> listOfService){
        loadStores(listOfService);
    }

    @Override
    public void callBackError(String messageError){
        errorOrLoading = true;
        load = false;
        this.info_fragment = new Error_fragment(messageError);
        changeFragment(this.info_fragment);
    }

    private void loadStores(final ArrayList<Service> listOfServices){
        this.listOfServices = listOfServices;
        thisStore.setListOfServices(listOfServices);
        listFragment = new List_Services_fragment(thisStore, this);
        changeFragment(listFragment);
    }


}
