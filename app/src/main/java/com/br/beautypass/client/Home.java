package com.br.beautypass.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.MenuItem;

import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.fragments.details.store.Detail_store_fragment;
import com.br.beautypass.client.fragments.menu.Calendar_fragment;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.menu.Home_fragment;
import com.br.beautypass.client.fragments.menu.Map_fragment;
import com.br.beautypass.client.fragments.menu.Options_fragment;
import com.br.beautypass.manager.UserManager;
import com.br.beautypass.util.InterfaceUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Stack;

public class Home extends AppCompatActivity {

    //Garante que só haja um object home
    private static Home this_home;

    public static Home getHome(){
        return this_home;
    }

    //Navegador entre fragmentos
    private BottomNavigationView bottomNavigationView;

    //Fragmento selecionado
    private Fragment_menu fragmentSelected;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.this_home = this;

        if (savedInstanceState == null) {

            // Instancia o primeiro fragmento
            Home_fragment home_fragment = Home_fragment.getIntance();
            home_fragment.setFragmentId(R.id.menu_button_home);
            fragmentSelected = home_fragment;

            // Aciona o primeiro fragmento
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.flContainer, home_fragment.getFragment());

            ft.commit();
        }

        //Ação de troca de item do menu
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        //Metodo que controla a alteração dos menus
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment_menu new_fragment = null;
                Bundle bundle = new Bundle();

                switch (item.getItemId()) {
                    case R.id.menu_button_home:
                        new_fragment = Home_fragment.getIntance();
                        break;
                    case R.id.menu_button_map:
                        new_fragment = Map_fragment.getIntance();
                         break;
                    case R.id.menu_button_calendar:
                        new_fragment = Calendar_fragment.getIntance();
                        break;
                    case R.id.menu_button_options:
                        new_fragment = Options_fragment.getIntance();
                        break;
                }

                new_fragment.setFragmentId(item.getItemId());
                new_fragment.setArguments(bundle);

                //Chama o novo fragmento
                if (new_fragment != null) {
                    //Valida se o fragmento está apto a carregar
                    changeFragment(new_fragment);
                    return true;
                }
                return false;
            }
        });

    }

    /**
     * Muda o fragmento
     * @param new_fragment
     */
    public void changeFragment(Fragment_menu new_fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, new_fragment.getFragment()) // replace flContainer
                .addToBackStack(null)
                .commit();
        this.fragmentSelected = new_fragment;
    }

    /**
     * Atualiza o fragmento caso esteja selecionado
     * @param new_fragment
     */
    public void updateFragmentIfSelected(Fragment_menu new_fragment){
        if (fragmentSelected.getFragmentId() == new_fragment.getFragmentId()) {
            changeFragment(new_fragment);
        }
    }

    public int getSelectedId(){
        return this.fragmentSelected.getFragmentId();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        saveLocaleUser();
        fragmentSelected.onRequestPermissionsResult();
    }

    private void saveLocaleUser(){
        //Obtem a ultima localização do user
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            UserManager.getInstance().setLocaleUser(location.getLatitude(), location.getLongitude());
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                InterfaceUtils.showMensage("Erro ao obter os dados de localização", Home.getHome().getApplicationContext());
                            }
                        }
                );
    }

    @Override
    public void onBackPressed() {
        if (this.fragmentSelected instanceof Details_Element){
            ((Details_Element)this.fragmentSelected).backToFatherWindow();
        }else{
            super.onBackPressed();
        }
    }

}
