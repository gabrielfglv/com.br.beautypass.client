package com.br.beautypass.client.fragments.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListStoreAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.manager.StoreManager;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.Store;

import java.util.ArrayList;

public class Filtred_Type_Serice extends Details_Element {

    private Category thisCategory;
    private ArrayList<Store> listOfStores;

    public Filtred_Type_Serice(Category thisCategory, Fragment_menu lastWindow){
        this.thisCategory = thisCategory;
        super.lastWindow = lastWindow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_list_stores_filtred_type, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(thisCategory.getName());
        listOfStores = StoreManager.getInstance().getHomeConfiguration().getListOfStoresFiltredByType(thisCategory.getId());

        ListView listViewStores = (ListView) view.findViewById(R.id.listStoresView);
        ListStoreAdapter adapter = new ListStoreAdapter(listOfStores,
                this.getActivity());

        listViewStores.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openDetails(listOfStores.get(position));
            }
        });

        //Altera a altura da lista
        ViewGroup.LayoutParams params = listViewStores.getLayoutParams();
        params.height = 236 * listOfStores.size();
        listViewStores.setLayoutParams(params);
        listViewStores.setScrollContainer(false);
        listViewStores.setAdapter(adapter);

        ImageView backImage = (ImageView) view.findViewById(R.id.back);
        backImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backToFatherWindow();
            }
        });

    }

}
