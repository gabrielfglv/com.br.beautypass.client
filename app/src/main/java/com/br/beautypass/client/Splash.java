package com.br.beautypass.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import com.br.beautypass.manager.PropertiesController;
import com.br.beautypass.manager.UserManager;
import com.br.beautypass.util.InterfaceUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class Splash extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Inicia as propriedades de ambiente
        PropertiesController.setPropertie("url_server", getResources().getString(R.string.url_server));

        if (InterfaceUtils.hasMapPermitions(this)){
            saveLocaleUser();
        }else {
            InterfaceUtils.getPermissions(this);
        }
    }

    private void goToHome(String message){

        //Obtem os dados do user
        UserManager.getInstance().loadUser(this);

        InterfaceUtils.showMensage(message, this.getBaseContext());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intentLogin = new Intent(
                        Splash.this, Home.class
                );
                startActivity(intentLogin);
                finish();
            }
        }, 4000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        saveLocaleUser();
    }

    private void saveLocaleUser(){
        //Obtem a ultima localização do user
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            UserManager.getInstance().setLocaleUser(location.getLatitude(), location.getLongitude());
                            goToHome("Localização obtida com sucesso");
                        }else{
                            goToHome("Erro ao obter a localização");
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                goToHome("Erro ao obter a localização");
                            }
                        }
                );
    }

}
