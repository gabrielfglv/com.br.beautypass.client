package com.br.beautypass.client.fragments.details.services;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListServiceOptionSelectedAdapter;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.util.CalcUtils;
import com.br.beautypass.util.MVPUtils;

import java.util.ArrayList;

public class Confirm_service_selected extends Details_Element {

    private ListServiceOptionSelectedAdapter listAdapter;
    private Details_service detailsPage;

    public Confirm_service_selected(Details_service detailsPage) {
        this.detailsPage = detailsPage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_confirm_sevice_fragment, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        final ListView listViewOptions = (ListView) view.findViewById(R.id.listServiceOptions);
        TextView value = (TextView) view.findViewById(R.id.total_service);
        value.setText("Total: "+ CalcUtils.getFormatedValueMoney(detailsPage.getTotalValue()));

        ArrayList<ServiceOption> optionsSelected = detailsPage.getOptionsSelected();

        ImageView imageStore = (ImageView) view.findViewById(R.id.image_store);
        imageStore.setImageResource(MVPUtils.getImageStoreById(detailsPage.getStore().getIdImage()));

        //Datas
        TextView date = (TextView) view.findViewById(R.id.date_confirm);
        TextView time = (TextView) view.findViewById(R.id.time_confirm);

        date.setText("Data: "+ detailsPage.getDateSelected().toString());
        time.setText("Hora: "+ detailsPage.getTimeSelected().getTime());

        TextView titleStore = (TextView) view.findViewById(R.id.title_store);
        TextView adress = (TextView) view.findViewById(R.id.local);

        titleStore.setText(detailsPage.getStore().getTitle());
        adress.setText(detailsPage.getStore().getAdressFormated());

        //Altera a altura da lista
        ViewGroup.LayoutParams params = listViewOptions.getLayoutParams();
        params.height = 170 * optionsSelected.size();
        listViewOptions.setLayoutParams(params);
        listViewOptions.setScrollContainer(false);

        listAdapter = new ListServiceOptionSelectedAdapter(detailsPage, optionsSelected);
        listViewOptions.setAdapter(listAdapter);

    }
}

