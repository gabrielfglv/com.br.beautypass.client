package com.br.beautypass.client.fragments.infoFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.Fragment_menu;

public class Error_fragment extends Fragment_menu {

    private TextView errorMessage;
    private String erroMessageTxt = null;

    public Error_fragment(String errorMessage){
        erroMessageTxt = errorMessage;
    }


    public void setMessageError(String error){
        if (errorMessage != null) {
            errorMessage.setText(error);
        }else{
            this.erroMessageTxt = error;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        errorMessage = (TextView) view.findViewById(R.id.errorMessage);

        if (erroMessageTxt != null){
            setMessageError(erroMessageTxt);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_error_fragment, parent, false);
    }

}