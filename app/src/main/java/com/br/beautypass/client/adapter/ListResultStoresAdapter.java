package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.search.Search_by_title;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.Store;
import com.br.beautypass.util.MVPUtils;

import java.util.List;

public class ListResultStoresAdapter extends BaseAdapter {

    private List<Store> listOfStores;
    private final Search_by_title act;

    public ListResultStoresAdapter(List<Store> stores, Search_by_title act) {
        this.listOfStores = stores;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listOfStores.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfStores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_stores_and_service, parent, false);
        final Store store = listOfStores.get(position);

        RelativeLayout relativeStoreData = (RelativeLayout) view.findViewById(R.id.relativeLayout);
        relativeStoreData.setTag(store);
        relativeStoreData.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
               RelativeLayout relativeLayout = (RelativeLayout)v;
               Store store = (Store)relativeLayout.getTag();
               openStore(store);
            }
        });

        //pegando as referencias das Views
        TextView nome = (TextView) view.findViewById(R.id.title);
        TextView description = (TextView) view.findViewById(R.id.description);
        ImageView imagem = (ImageView) view.findViewById(R.id.image);

        //populando as Views
        nome.setText(store.getTitle());
        description.setText(store.getCategoryDescription());

        imagem.setImageResource(MVPUtils.getImageStoreById(store.getIdImage()));

        ListView listServices = (ListView) view.findViewById(R.id.services);
        if (store.getListOfServices() != null && !store.getListOfServices().isEmpty()) {

            //Altera a altura da lista
            ViewGroup.LayoutParams params = listServices.getLayoutParams();
            params.height = 150 * store.getListOfServices().size();
            listServices.setLayoutParams(params);
            listServices.setScrollContainer(false);

            ListServiceSearchAdapter listAdapter = new ListServiceSearchAdapter(store.getListOfServices(), act);
            listServices.setAdapter(listAdapter);

            listServices.setTag(store);
            listServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent sendIntent = new Intent(Home.getHome(), Details_service.class);
                    Bundle bundleInf= new Bundle();
                    bundleInf.putSerializable("store", store);
                    bundleInf.putSerializable("service", store.getListOfServices().get(position));
                    sendIntent.putExtras(bundleInf);
                    act.startActivity(sendIntent);

                }

            });

        }else{
            listServices.setVisibility(View.GONE);
        }

        return view;
    }

    public void openStore(Store store){
        act.openStore(store);
    }

}
