package com.br.beautypass.client.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.br.beautypass.client.Contact;
import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.fragments.menu.Calendar_fragment;
import com.br.beautypass.client.dialog.DialogRaitingService;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.util.InterfaceUtils;
import com.br.beautypass.util.MVPUtils;

import java.util.List;

public class ListCalendarAdapter  extends BaseAdapter {

    private List<ScheduledService> listOfOptions;
    private final Calendar_fragment calendar_fragment;

    public ListCalendarAdapter(List<ScheduledService> listOfOptions, Calendar_fragment calendar_fragment) {
        this.listOfOptions = listOfOptions;
        this.calendar_fragment = calendar_fragment;
    }

    @Override
    public int getCount() {
        return listOfOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfOptions.get(position);
    }

    @Override
    public boolean isEmpty(){ return listOfOptions.isEmpty(); }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = calendar_fragment.getLayoutInflater().inflate(R.layout.activity_item_list_calendar, parent, false);
        ScheduledService service = listOfOptions.get(position);

        //pegando as referencias das Views
        TextView titleStore = (TextView) view.findViewById(R.id.storeName);
        TextView status = (TextView) view.findViewById(R.id.status);
        TextView cod = (TextView) view.findViewById(R.id.idService);
        TextView date = (TextView) view.findViewById(R.id.dateAgendamento);
        TextView details = (TextView) view.findViewById(R.id.buttonAcompanhar);
        details.setTag(service);
        TextView help = (TextView) view.findViewById(R.id.buttonAjuda);
        help.setTag(service);

        ImageView imageStore = (ImageView) view.findViewById(R.id.imageStore);

        titleStore.setText(service.getStore().getTitle());
        cod.setText("Atendimento "+service.getId());
        date.setText(service.getDateString());

        status.setText("Status: "+service.getStatusStr());

        if (service.isExecuted()){
            details.setText("Detalhes");
        }

        RelativeLayout dateMarket = (RelativeLayout) view.findViewById(R.id.rowDate);
        TableRow rowScore = (TableRow) view.findViewById(R.id.rowScore);

        if (service.isExecuted()){
            dateMarket.setVisibility(View.GONE);

            ImageView scissor1 = (ImageView) view.findViewById(R.id.scissor1);
            ImageView scissor2 = (ImageView) view.findViewById(R.id.scissor2);
            ImageView scissor3 = (ImageView) view.findViewById(R.id.scissor3);
            ImageView scissor4 = (ImageView) view.findViewById(R.id.scissor4);
            ImageView scissor5 = (ImageView) view.findViewById(R.id.scissor5);

            LinearLayoutCompat scissors = (LinearLayoutCompat) view.findViewById(R.id.scissors);
            TextView titleRanting = (TextView) view.findViewById(R.id.statusDescription1);
            if (service.getConfirmed() == 1){
                scissors.setVisibility(View.GONE);
                titleRanting.setText(calendar_fragment.getString(R.string.service_dont_realize));
            }else {
                status.setVisibility(View.GONE);
            }

            if (service.getScore() > -1){

                @DrawableRes int imageScissorDone = R.drawable.scissors_done;

                scissor1.setImageResource(imageScissorDone);

                if (service.getScore() >= 2)
                    scissor2.setImageResource(imageScissorDone);

                if (service.getScore() >= 3)
                    scissor3.setImageResource(imageScissorDone);

                if (service.getScore() >= 4)
                    scissor4.setImageResource(imageScissorDone);

                if (service.getScore() >= 5)
                    scissor5.setImageResource(imageScissorDone);

            }else{

                scissor1.setTag(service);
                scissor1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(1,v);
                    }
                });

                scissor2.setTag(service);
                scissor2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(2,v);
                    }
                });

                scissor3.setTag(service);
                scissor3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(3,v);
                    }
                });

                scissor4.setTag(service);
                scissor4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(4,v);
                    }
                });

                scissor5.setTag(service);
                scissor5.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(5,v);
                    }
                });

            }

        }else{
            rowScore.setVisibility(View.GONE);
        }

        imageStore.setImageResource(MVPUtils.getImageStoreById(service.getStore().getIdImage()));

        /**
         * Eventos de clique nos botões
         */

        details.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToDetails(v);
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToHelp(v);
            }
        });


        return view;
    }

    private void goToDetails(View view){
        if (view instanceof TextView){
            TextView txtView = (TextView) view;
            ScheduledService scheduledService = (ScheduledService) txtView.getTag();
            calendar_fragment.openDetails(scheduledService);
        }
    }

    private void goToHelp(View view){
        if (calendar_fragment.isOffline()){
            InterfaceUtils.showMensage(calendar_fragment.getString(R.string.offiline_message), Home.getHome());
        }else if (view instanceof TextView){
            TextView txtView = (TextView) view;
            ScheduledService scheduledService = (ScheduledService) txtView.getTag();

            //Abre a janela de ajuda
            Intent sendIntent = new Intent(Home.getHome(), Contact.class);
            Bundle bundleInf= new Bundle();
            bundleInf.putSerializable("idSevice", scheduledService.getId());
            sendIntent.putExtras(bundleInf);
            calendar_fragment.startActivity(sendIntent);
        }
    }

    private void avaliableService(int val, View view){
        if (calendar_fragment.isOffline()){
            InterfaceUtils.showMensage(calendar_fragment.getString(R.string.offiline_message), Home.getHome());
        }else {
            ScheduledService scheduledService = (ScheduledService) ((ImageView) view).getTag();

            DialogRaitingService dialog = new DialogRaitingService(val, scheduledService);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    DialogRaitingService dialogRaitingService = (DialogRaitingService) dialog;
                    //if (dialogRaitingService.isSucess())
                        refreshItem(dialogRaitingService.getServiceId(), dialogRaitingService.getFinalValue());
                }
            });
            dialog.show();

        }
    }

    private void refreshItem(int id, int value){
        if (value != -1) {
            for (int index = 0; index < listOfOptions.size(); index++) {
                if (listOfOptions.get(index).getId() == id) {
                    listOfOptions.get(index).setScore(value);
                    break;
                }
            }
            this.notifyDataSetChanged();
        }
    }

}
