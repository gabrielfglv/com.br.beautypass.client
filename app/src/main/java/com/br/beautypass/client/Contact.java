package com.br.beautypass.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.br.beautypass.client.service.Default_Message;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.manager.ContactManager;
import com.br.beautypass.model.Service;
import com.br.beautypass.model.Store;

public class Contact extends AppCompatActivity {

    private int idService = -1;

    private RelativeLayout loading;
    private EditText editText;
    private Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        editText = (EditText) findViewById(R.id.textArea);
        loading = (RelativeLayout) findViewById(R.id.loading);
        buttonSubmit = (Button) findViewById(R.id.submit);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        TextView textNumber = findViewById(R.id.numberAtend);
        ImageView backButton = (ImageView) findViewById(R.id.back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Recebe as configurações
        Intent intentRecebida = getIntent();
        if (intentRecebida!=null) {
            Bundle bundle = intentRecebida.getExtras();
            if (bundle != null) {

                Object idSericeObj = bundle.get("idSevice");

                if (idSericeObj != null){

                    try{

                        idService = Integer.parseInt(idSericeObj.toString());
                        textNumber.setText("Atendimento "+idService);

                    }catch (Exception e){
                        callBackError("Erro no processamento dos parametros");
                    }

                }else{
                    callBackError("Erro na passagem dos parametros");
                }

            }
        }
    }

    private void sendMessage(){
        loading.setVisibility(View.VISIBLE);
        editText.setVisibility(View.GONE);
        buttonSubmit.setVisibility(View.GONE);
        ContactManager.getInstance().process(editText.getText().toString(), idService, this);
    }

    public void callBackSucess(){
        this.finish();
        Intent sendIntent = new Intent(Contact.this, Default_Message.class);
        Bundle bundleInf= new Bundle();
        bundleInf.putSerializable("error", false);
        bundleInf.putSerializable("msg", "Sucesso ao  enviar o contato!");
        sendIntent.putExtras(bundleInf);
        startActivity(sendIntent);
    }

    public void callBackError(String message){
        this.finish();
        Intent sendIntent = new Intent(Contact.this, Default_Message.class);
        Bundle bundleInf= new Bundle();
        bundleInf.putSerializable("error", true);
        bundleInf.putSerializable("msg", message);
        sendIntent.putExtras(bundleInf);
        startActivity(sendIntent);
    }


}
