package com.br.beautypass.client.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.br.beautypass.client.R;
import com.br.beautypass.client.service.Details_service;
import com.br.beautypass.model.Category;
import com.br.beautypass.model.ServiceOption;
import com.br.beautypass.util.CalcUtils;

import java.util.ArrayList;

public class ListServiceOptionSelectedAdapter extends BaseAdapter {

    private ArrayList<ServiceOption> listOfServiceOption;
    private final Activity act;

    public ListServiceOptionSelectedAdapter(Activity act, ArrayList<ServiceOption> listOfServiceOption) {
        this.listOfServiceOption = listOfServiceOption;
        this.act = act;
    }

    @Override
    public int getCount() {
        return listOfServiceOption.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfServiceOption.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_item_list_option_service_selected, parent, false);
        ServiceOption serviceOption = listOfServiceOption.get(position);

        //pegando as referencias das Views
        TextView name = (TextView) view.findViewById(R.id.title);
        TextView valor = (TextView) view.findViewById(R.id.value);

        name.setText(serviceOption.getTitle());
        valor.setText(CalcUtils.getFormatedValueMoney(serviceOption.getValor()));

        return view;
    }
}
