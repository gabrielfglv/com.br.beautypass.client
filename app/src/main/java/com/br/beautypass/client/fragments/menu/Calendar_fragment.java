package com.br.beautypass.client.fragments.menu;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListCalendarAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;

import com.br.beautypass.client.fragments.details.calendar.Detail_calendar;
import com.br.beautypass.client.fragments.infoFragment.Loading_fragment;

import com.br.beautypass.manager.DateUserManager;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.util.InterfaceUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Calendar_fragment extends Fragment_menu {


    private static Calendar_fragment this_fragment = null;

    private ListView list;
    private TextView errorMessage;
    private ImageView imageError;

    private boolean offline = false;

    private int itemSelected = 0;

    private ArrayList<ScheduledService> listOfServices;
    private ListCalendarAdapter list_previus;
    private ListCalendarAdapter list_scheduled;

    public static Calendar_fragment getIntance(){
        if (this_fragment == null){
            this_fragment = new Calendar_fragment();
        }
        return this_fragment;
    }

    @Override
    public boolean isReady(){
        load = false;
        super.info_fragment = new Loading_fragment();
        super.info_fragment.setFragmentId(fragmentId);
        return listOfServices != null;
    }

    @Override
    protected void processRequirements(){
        super.errorOrLoading = true;
        DateUserManager.getInstance().initProcess(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        changeList(itemSelected);
    }

    @Override
    public void openDetails(ScheduledService sheduledService){
        listOfServices = null;
        super.openDetails(sheduledService);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_calendar_fragment, parent, false);
    }

    public void callBackReady(ArrayList<ScheduledService> listOfServices, boolean offline){
        if (listOfServices == null){
            errorOrLoading = true;
            callBackError("Erro ao obter os serviços");
        }else{
            this.listOfServices = listOfServices;
            super.load = true;
            super.errorOrLoading = false;
            this.offline = offline;
            callBackReady();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.errorOrLoading = false;
        super.load = false;

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);

        ArrayList<ScheduledService> listOfServicesPrevius = new ArrayList<>();
        ArrayList<ScheduledService> listOfServicesScheduled = new ArrayList<>();

        for(ScheduledService scheduledService : listOfServices){
            if (scheduledService.isExecuted()){
                listOfServicesPrevius.add(scheduledService);
            }else{
                listOfServicesScheduled.add(scheduledService);
            }
        }

        list_previus = new ListCalendarAdapter(listOfServicesPrevius, this);
        list_scheduled = new ListCalendarAdapter(listOfServicesScheduled, this);

        list = (ListView) view.findViewById(R.id.listCalendar);
        errorMessage = (TextView) view.findViewById(R.id.message);
        imageError = (ImageView) view.findViewById(R.id.attencion_image);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab != null && (tab.getPosition() == 0 || tab.getPosition() == 1))
                    changeList(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //changeList(itemSelected);
        TabLayout.Tab tab = tabLayout.getTabAt(itemSelected);
        tab.select();

        if (offline)
            InterfaceUtils.showMensage(this.getString(R.string.offiline_message), Home.getHome());

    }

    private void changeErrorMessage(boolean show, String message){
        if (show){
            errorMessage.setVisibility(View.VISIBLE);
            imageError.setVisibility(View.VISIBLE);
            errorMessage.setText(message);
        }else{
            errorMessage.setVisibility(View.INVISIBLE);
            imageError.setVisibility(View.INVISIBLE);
        }
    }

    public void changeList(int positionFragment){

        int sizeArr;
        int sizeList;
        if (positionFragment == 0){
            itemSelected = 0;
            if (list_scheduled.isEmpty()){
                changeErrorMessage(true, getActivity().getString(R.string.no_services_marked));
            }else{
                changeErrorMessage(false, null);
            }
            sizeArr = list_scheduled.getCount();
            list.setAdapter(list_scheduled);
            sizeList = 480;
        }else{
            itemSelected = 1;
            if (list_previus.isEmpty()){
                changeErrorMessage(true, getActivity().getString(R.string.no_previus_service));
            }else{
                changeErrorMessage(false, null);
            }
            list.setAdapter(list_previus);
            sizeArr = list_previus.getCount();
            sizeList = 560;
        }

        ViewGroup.LayoutParams params = list.getLayoutParams();
        params.height = sizeList * sizeArr;
        list.setLayoutParams(params);
        list.setScrollContainer(false);

    }

    @Override
    public void onPause() {
        this.listOfServices = null;
        super.onPause();
    }

    public boolean isOffline(){
        return offline;
    }

}
