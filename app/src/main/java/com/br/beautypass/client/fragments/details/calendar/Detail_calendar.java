package com.br.beautypass.client.fragments.details.calendar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.br.beautypass.client.Contact;
import com.br.beautypass.client.Home;
import com.br.beautypass.client.R;
import com.br.beautypass.client.adapter.ListServiceOptionSelectedAdapter;
import com.br.beautypass.client.fragments.Fragment_menu;
import com.br.beautypass.client.fragments.details.Details_Element;
import com.br.beautypass.client.fragments.menu.Calendar_fragment;
import com.br.beautypass.client.dialog.DialogRaitingService;
import com.br.beautypass.model.ScheduledService;
import com.br.beautypass.util.CalcUtils;
import com.br.beautypass.util.InterfaceUtils;
import com.br.beautypass.util.MVPUtils;

public class Detail_calendar extends Details_Element{

    private ScheduledService thisService;
    private boolean raitingDone = false;
    private TextView titleScore;
    private ImageView scissor1,
            scissor2,
            scissor3,
            scissor4,
            scissor5;


    public Detail_calendar(ScheduledService thisService, Fragment_menu lastWindow){
        this.thisService = thisService;
        super.lastWindow = lastWindow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_details_calendar, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        /**
         * Ações de clique
         */

        ImageView backImage = (ImageView) view.findViewById(R.id.back);
        backImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backToFatherWindow();
            }
        });

        TextView help = (TextView) view.findViewById(R.id.buttonHelp);
        help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToHelp();
            }
        });



          /*

        Trecho que mostra as informações da loja

        */

        TextView titleStore = (TextView) view.findViewById(R.id.storeName);
        titleStore.setText(thisService.getStore().getTitle());

        TextView dateService = (TextView) view.findViewById(R.id.dateAgendamento);
        dateService.setText(thisService.getDateString());

        TextView email = (TextView) view.findViewById(R.id.email);
        email.setText("E-mail: "+thisService.getStore().getEmail());

        TextView tel = (TextView) view.findViewById(R.id.tel);
        tel.setText("Telefone: "+thisService.getStore().getTelefone());

        TextView adress = (TextView) view.findViewById(R.id.adress);
        adress.setText("Endereço: "+thisService.getStore().getAdress());

        /*

        DADOS DO SERVIÇO

         */

        TextView idService = (TextView) view.findViewById(R.id.numberAtend);
        idService.setText("Atendimento: "+thisService.getId());

        TextView totalValue = (TextView) view.findViewById(R.id.value);
        totalValue.setText(CalcUtils.getFormatedValueMoney(thisService.getValue()));

        ListView listOptionsSelected = (ListView) view.findViewById(R.id.listServiceOptions);

        //Altera a altura da lista
        ViewGroup.LayoutParams params = listOptionsSelected.getLayoutParams();
        params.height = 170 * thisService.getListOfOptions().size();
        listOptionsSelected.setLayoutParams(params);
        listOptionsSelected.setScrollContainer(false);

        ListServiceOptionSelectedAdapter listAdapter = new ListServiceOptionSelectedAdapter(this.getActivity(), thisService.getListOfOptions());
        listOptionsSelected.setAdapter(listAdapter);

        /*

        Trecho que mostra a avaliação ou confirmação do serviço

        */

        TableRow avaliableService = (TableRow) view.findViewById(R.id.rowScore);
        TableRow statusService = (TableRow) view.findViewById(R.id.rowMessageFuture);

        if (thisService.isExecuted()){

            statusService.setVisibility(View.GONE);
            titleScore = (TextView) view.findViewById(R.id.statusDescription1);

            scissor1 = (ImageView) view.findViewById(R.id.scissor1);
            scissor2 = (ImageView) view.findViewById(R.id.scissor2);
            scissor3 = (ImageView) view.findViewById(R.id.scissor3);
            scissor4 = (ImageView) view.findViewById(R.id.scissor4);
            scissor5 = (ImageView) view.findViewById(R.id.scissor5);

            if (thisService.getScore() > -1){
                titleScore.setText(getActivity().getString(R.string.avaliation_done));
                refreshItem(thisService.getScore());

            }else{
                titleScore.setText(getActivity().getString(R.string.avaliation));

                scissor1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(1);
                    }
                });


                scissor2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(2);
                    }
                });


                scissor3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(3);
                    }
                });


                scissor4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(4);
                    }
                });


                scissor5.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        avaliableService(5);
                    }
                });

            }

            LinearLayoutCompat scissors = (LinearLayoutCompat) view.findViewById(R.id.scissors);
            if (thisService.getConfirmed() == 1){
                scissors.setVisibility(View.GONE);
                titleScore.setText(lastWindow.getString(R.string.service_dont_realize));
            }

        }else{

            avaliableService.setVisibility(View.GONE);
            ImageView statusImage = (ImageView) view.findViewById(R.id.statusImage);
            TextView statusDescription = (TextView) view.findViewById(R.id.statusDescription);

            switch (thisService.getConfirmed()){
                case -1:
                    statusImage.setImageResource(R.drawable.calendario2);
                    statusDescription.setText(getActivity().getString(R.string.service_waiting));
                    break;
                case 0:
                    statusImage.setImageResource(R.drawable.calendario3);
                    statusDescription.setText(getActivity().getString(R.string.service_confirmed));
                    break;
                default:
                    statusImage.setImageResource(R.drawable.calendario1);
                    statusDescription.setText(getActivity().getString(R.string.service_denided));
                    break;
            }


        }

        //Imagem aleatoria da loja
        ImageView imageStore = (ImageView) view.findViewById(R.id.imageStore);
        imageStore.setImageResource(MVPUtils.getImageStoreById(thisService.getStore().getIdImage()));

    }

    private void goToHelp(){
        if (lastWindow instanceof  Calendar_fragment &&
                ((Calendar_fragment)lastWindow).isOffline()) {
            InterfaceUtils.showMensage(this.getString(R.string.offiline_message), Home.getHome());
        }else {
            //Abre a janela de ajuda
            Intent sendIntent = new Intent(Home.getHome(), Contact.class);
            Bundle bundleInf = new Bundle();
            bundleInf.putSerializable("idSevice", thisService.getId());
            sendIntent.putExtras(bundleInf);
            lastWindow.startActivity(sendIntent);
        }
    }

    private void avaliableService(int val){
        if (lastWindow instanceof  Calendar_fragment &&
                ((Calendar_fragment)lastWindow).isOffline()) {
            InterfaceUtils.showMensage(this.getString(R.string.offiline_message), Home.getHome());
        }else {
            if (!raitingDone) {
                final DialogRaitingService dialog = new DialogRaitingService(val, thisService);
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        DialogRaitingService dialogRaitingService = (DialogRaitingService) dialog;
                        refreshItem(dialogRaitingService.getFinalValue());
                    }
                });
            }
        }
    }

    private void refreshItem(int value){

        if (value != -1) {

            raitingDone = true;
            titleScore.setText(getActivity().getString(R.string.avaliation_done));

            @DrawableRes int imageScissorDone = R.drawable.scissors_done;
            scissor1.setImageResource(imageScissorDone);

            if (value >= 2)
                scissor2.setImageResource(imageScissorDone);

            if (value >= 3)
                scissor3.setImageResource(imageScissorDone);

            if (value >= 4)
                scissor4.setImageResource(imageScissorDone);

            if (value >= 5)
                scissor5.setImageResource(imageScissorDone);



        }

    }

}
