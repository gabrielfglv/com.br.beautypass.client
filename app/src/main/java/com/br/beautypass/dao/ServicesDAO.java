package com.br.beautypass.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ServicesDAO {

    private SQLiteDatabase sqlLite;
    private DBFactory database;

    public ServicesDAO(Context context){
        database = new DBFactory(context);
    }

    public JSONObject getJSONCached(String id){
        String selectQuery = "SELECT * FROM CACHE_JSON WHERE id = '"+id+"'";

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        JSONObject jsonToReturn = null;
        if(cursor.getCount() > 0) {
            StringBuilder sb = new StringBuilder();
            cursor.moveToFirst();

            String response = cursor.getString(cursor.getColumnIndex("content"));
            JSONParser jsonParser = new JSONParser();

            try{
                jsonToReturn = (JSONObject) jsonParser.parse(response);
            }catch (Exception e){
                jsonToReturn = null;
                e.printStackTrace();
            }

        }

        cursor.close();
        db.close();

        return jsonToReturn;
    }

    public boolean insertJSON(String key, JSONObject JsonObj){
        ContentValues values;

        boolean sucess = true;
        try {
            SQLiteDatabase db = database.getWritableDatabase();
            values = new ContentValues();

            values.put("id", key);
            values.put("content", JsonObj.toString());

            long resultado = db.insert("CACHE_JSON", null, values);

            db.close();
            if (resultado == -1)
                sucess = false;

        } catch (Exception e){
            System.out.println(e);
            sucess = false;
        }

        return sucess;
    }

    public boolean updateJSON(String key, JSONObject jsonObj){
        ContentValues valores;

        boolean sucess = false;

        try {
            SQLiteDatabase db = database.getWritableDatabase();

            valores = new ContentValues();
            String where =  "id= '" +key+"'";
            valores.put("content", jsonObj.toString());
            db.update("CACHE_JSON",valores,where,null);
            db.close();

        } catch (Exception e){
            sucess = false;
            System.out.println(e);
        }

        return sucess;
    }

}
