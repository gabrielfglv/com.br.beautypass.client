package com.br.beautypass.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Service implements Serializable{

    private String title;
    private int id,
            position;
    private String description;
    private double value;
    private boolean valid,
            hightlight,
            multiOption;
    private Category segment;
    private Category typeService;
    private ArrayList<ServiceOption> serviceOption;
    private Long duration;
    private Store store;

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public int getId() {
        return id;
    }
    public void setId(String id) {
        this.id = Integer.parseInt(id);
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public double getValue() {
        return value;
    }
    public void setValue(double value) {
        this.value = value;
    }
    public boolean isValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return id+":{name:\""+title+"\", description:\""+description+"\",value:"+value+
                ", valid:"+valid+"}";
    }
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    public boolean isHightlight() {
        return hightlight;
    }
    public void setHightlight(boolean hightlight) {
        this.hightlight = hightlight;
    }
    public boolean isMultiOption() {
        return multiOption;
    }
    public void setMultiOption(boolean multiOption) {
        this.multiOption = multiOption;
    }
    public Category getTypeService() {
        return typeService;
    }
    public void setTypeService(int typeService) {
        this.typeService = new Category(typeService);
    }
    public void setTypeService(Category typeService) {
        this.typeService = typeService;
    }
    public ArrayList<ServiceOption> getServiceOption() {
        return serviceOption;
    }
    public void setServiceOption(ArrayList<ServiceOption> serviceOption) {
        this.serviceOption = serviceOption;
    }
    public Category getSegment() {
        return segment;
    }
    public void setSegment(Category segment) {
        this.segment = segment;
    }
    public void setSegment(int segmentId) {
        this.segment = new Category(segmentId);
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
