package com.br.beautypass.model;


import java.io.Serializable;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class GoogleCalendarEvent implements Serializable {

    private long startMillis,
            endMillis;
    private String title,
            description,
            locale;

    public void setTime(Long startMillis, Long duration){
        this.startMillis = startMillis;
        Calendar endTimeCalendar = Calendar.getInstance();
        endTimeCalendar.setTimeInMillis(startMillis+duration);
        endMillis = endTimeCalendar.getTimeInMillis();
    }

    public long getStartMillis() {
        return startMillis;
    }

    public long getEndMillis() {
        return endMillis;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocale() {
        return locale;
    }

    public void setTexts(Service service, Store store) {
        this.title = store.getTitle() + " - " + service.getTitle();
        this.description = "Procedimento "+service.getTitle()+" em "+store.getTitle()+
                ": "+service.getDescription();
        this.locale = store.getCoordX()+","+store.getCoordY();
    }



}
