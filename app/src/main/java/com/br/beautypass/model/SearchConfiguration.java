package com.br.beautypass.model;

import java.util.ArrayList;

public class SearchConfiguration {

    private ArrayList<Store> resultStores = null;
    private int limitServices = -1;

    public ArrayList<Store> getResultStores() {
        return resultStores;
    }

    public void setResultStores(ArrayList<Store> resultStores) {
        this.resultStores = resultStores;
    }

    public int getLimitServices() {
        return limitServices;
    }

    public boolean isEmpty(){
        return resultStores == null || resultStores.isEmpty();
    }

    public void setLimitServices(int limitServices) {
        this.limitServices = limitServices;
    }
}
