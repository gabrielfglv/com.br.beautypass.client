package com.br.beautypass.model;

import com.br.beautypass.util.CalcUtils;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Store implements Serializable {

    private int id,
            priceLvl,
            qtRaitings,
            idImage;
    private String title,
            cnpj,
            introduction,
            description,
            telefone,
            adress,
            complement,
            cep,
            email;

    private float coordX,
            coordY,
            score;
    private double distanceFromDestiny = -1;
    private boolean valid;
    private Date createDate;
    private Category category;
    private ArrayList<Service> listOfServices;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getCnpj() {
        return cnpj;
    }
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    public String getIntroduction() {
        return introduction;
    }
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public String getAdress() {
        return adress;
    }
    public void setAdress(String adress) {
        this.adress = adress;
    }
    public String getCep() {
        return cep;
    }
    public void setCep(String cep) {
        this.cep = cep;
    }
    public float getCoordX() {
        return coordX;
    }
    public void setCoordX(float coordX) {
        this.coordX = coordX;
    }
    public float getCoordY() {
        return coordY;
    }
    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }
    public boolean isValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getComplement() {
        return complement;
    }
    public void setComplement(String complement) {
        this.complement = complement;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public double getDistanceFromDestiny() {
        return distanceFromDestiny;
    }
    public void setDistanceFromDestiny(double distanceFromDestiny) {
        this.distanceFromDestiny = distanceFromDestiny;
    }
    public String getDistanceFromDestinyStr(){
        String strToReturn = "";
        double distance = this.distanceFromDestiny;
        if (distance > -1) {
            //Valida se a distancia é em metros ou km
            String posfixDistance = distance > 1 ? "km" : "m";
            double distanceFromDestiny = distance > 1 ? distance : distance*1000;
            distance = CalcUtils.roundNumber(distanceFromDestiny, 2);
            strToReturn = distance+" "+posfixDistance;
        }
        return strToReturn;
    }
    public LatLng getLatLng(){
        return new LatLng(coordX, coordY);
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public void setCategory(int id, String categoryName) {
        this.category = new Category(id, categoryName);
    }
    public void setCategory(int id, String categoryName, int idImage) {
        this.category = new Category(id, categoryName, idImage);
    }
    public Category getCategory() {
        return category;
    }
    public float getScore() {
        return score;
    }
    public void setScore(float score) {
        this.score = score;
    }
    public String getCategoryDescription(){
        String categoryDescription = this.category.getName();
        String distanceStr = getDistanceFromDestinyStr();
        if (!distanceStr.isEmpty()) {
            categoryDescription += " • "+distanceStr;
        }
        return categoryDescription;
    }

    public ArrayList<Service> getListOfServices() {
        return listOfServices;
    }

    public void setListOfServices(ArrayList<Service> listOfServices) {
        this.listOfServices = listOfServices;
    }
    public String getAdressFormated(){
        return adress + ", CEP: "+cep;
    }

    public int getPriceLvl() {
        return priceLvl;
    }

    public void setPriceLvl(int priceLvl) {
        this.priceLvl = priceLvl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getQtRaitings() {
        return qtRaitings;
    }

    public void setQtRaitings(int qtRaitings) {
        this.qtRaitings = qtRaitings;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

}
